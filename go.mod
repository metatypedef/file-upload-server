module file-upload-server

go 1.16

require (
	github.com/gorilla/mux v1.8.0
	github.com/hashicorp/go-hclog v0.15.0
	github.com/nicholasjackson/building-microservices-youtube/product-images v0.0.0-20200918064506-0260dd0e3676 // indirect
	github.com/nicholasjackson/env v0.6.0
)
