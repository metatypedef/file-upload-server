package handlers

import (
	"github.com/gorilla/mux"
	"github.com/hashicorp/go-hclog"
	"github.com/nicholasjackson/building-microservices-youtube/product-images/files"
	"io"
	"net/http"
	"path/filepath"
	"strconv"
)

type Images struct {
	log hclog.Logger
	store files.Storage
}

func NewImages(s files.Storage, l hclog.Logger) *Images {
	return &Images{log: l, store: s}
}


func (h *Images) Post(writer http.ResponseWriter, req *http.Request) {
	vars := mux.Vars(req)
	id := vars["id"]
	fileName := vars["filename"]

	h.log.Info("Handle POST", "id", id, "filename", fileName)

	if len(id) == 0 || len(fileName) == 0 {
		h.InvalidUrl(req.URL.String(), writer)
	}

	h.saveFile(id, fileName, writer, req.Body)
}

func (h *Images) PostMultipart(writer http.ResponseWriter, req *http.Request) {
	err := req.ParseMultipartForm(128 * 1024)
	if err != nil {
		h.log.Error("Bad request", "error", err)
		http.Error(writer, "Expected multipart form data", http.StatusBadRequest)
		return
	}

	id, idErr := strconv.Atoi(req.FormValue("id"))
	h.log.Info("Process form for id", "id", id)
	if idErr != nil {
		h.log.Error("Bad request", "error", err)
		http.Error(writer, "Expected integer id", http.StatusBadRequest)
		return
	}

	multiPartFile, fileHeader, err := req.FormFile("file")
	if err != nil {
		h.log.Error("Bad request", "error", err)
		http.Error(writer, "Expected file", http.StatusBadRequest)
		return
	}

	h.saveFile(req.FormValue("id"), fileHeader.Filename, writer, multiPartFile)
}

func (h *Images) InvalidUrl(uri string, writer http.ResponseWriter) {
	h.log.Error("Invalid path", "path", uri)
	http.Error(writer, "Invalid file path should be in format: /[id]/[filepath]", http.StatusBadRequest)
}

func (h *Images) saveFile(id, path string, writer http.ResponseWriter, read io.ReadCloser) {
	h.log.Info("Save file for product", "id", id, "path", path)

	fp := filepath.Join(id, path)
	err := h.store.Save(fp, read)
	if err != nil {
		h.log.Error("Unable to save file", err)
		http.Error(writer, "Unable to save file", http.StatusInternalServerError)
	}
}
