package main

import (
	"context"
	"file-upload-server/handlers"
	"github.com/gorilla/mux"
	"github.com/hashicorp/go-hclog"
	"github.com/nicholasjackson/building-microservices-youtube/product-images/files"
	"github.com/nicholasjackson/env"
	"log"
	"net/http"
	"os"
	"os/signal"
	"time"
)

var bindAddress = env.String("BIND_ADDRESS", false, ":7777", "Bind address for http server")
var logLevel = env.String("LOG_LEVEL", false, "debug", "Log output level for the server [debug, info, trace]")
var basePath = env.String("BASE_PATH", false, "./imagestore", "Base path to save images")

func main() {
	env.Parse()

	logger := hclog.New(
		&hclog.LoggerOptions{
			Name:  "product-images",
			Level: hclog.LevelFromString(*logLevel),
		},
	)

	// create a logger for the server from the default logger
	sl := logger.StandardLogger(&hclog.StandardLoggerOptions{InferLevels: true})

	maxSize := 1024*1000*5
	store, err := files.NewLocal(*basePath, maxSize)
	if err != nil {
		logger.Error("Unable to create storage", "error", err)
		os.Exit(1)
	}

	images := handlers.NewImages(store, logger)

	serveMux := mux.NewRouter()
	fileRouter := serveMux.PathPrefix("/file").Subrouter()

	postFileRouter := fileRouter.Methods(http.MethodPost).Subrouter()
	postFileRouter.HandleFunc("/{id:[0-9]+}/{filename:[a-zA-Z]+\\.[a-z]{3}}", images.Post)
	postFileRouter.HandleFunc("/multipart/{id:[0-9]+}/{filename:[a-zA-Z]+\\.[a-z]{3}}", images.PostMultipart)


	// get files
	getFileRouter := fileRouter.Methods(http.MethodGet).Subrouter()
	getFileRouter.Handle(
		"/images/{id:[0-9]+}/{filename:[a-zA-Z]+\\.[a-z]{3}}",
		http.StripPrefix("/file/images/", http.FileServer(http.Dir(*basePath))),
	)

	server := &http.Server{
		Addr:         *bindAddress,
		Handler:      serveMux,
		ErrorLog: sl,
		IdleTimeout:  120 * time.Second,
		ReadTimeout:  10 * time.Second,
		WriteTimeout: 10 * time.Second,
	}

	go func() {
		err := server.ListenAndServe()
		if err != nil {
			log.Fatal(err)
		}
	}()

	signalChan := make(chan os.Signal)
	signal.Notify(signalChan, os.Interrupt)
	signal.Notify(signalChan, os.Kill)

	sig := <-signalChan
	logger.Info("Received terminate and start gracefully shutdown", sig)

	ctx, _ := context.WithTimeout(context.Background(), 30*time.Second)
	server.Shutdown(ctx)

}
